import Link from "next/link";
import Image from "next/image";
import Head from "next/head";
import Layout from "../../component/Layout";

export default function FirstPost() {
  return (
    <Layout>
      <Head>
        <title>First Post</title>
      </Head>
      <Link href="/">
        <h1>
          <a> First Post </a>
        </h1>
      </Link>
      <Image src="/profile.jpeg" height={144} width={144} alt="Your Name" />

      <style jsx>{`
        h1 {
          color: red;
          font-size: 20px;
          margin-bottom: 30px;
        }
      `}</style>
    </Layout>
  );
}
